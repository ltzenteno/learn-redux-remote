/**
 * @author zenteno
 * @created 1/30/17
 */

export default function comments(state = [], action) {

    if(typeof action.postId !== 'undefined'){
        return {
            //take the current state
            ...state,
            //overwrite this post with a new one
            [action.postId] : postComments(state[action.postId], action)
        };
    }
    return state;
}

/**
 * Funcion reducer que actua como reducer composition (solo toma un slice del state)
 * @param state
 * @param action
 */
export function postComments(state = [], action){
    switch(action.type){
        case 'ADD_COMMENT':
            //return the new state with the new comment
            return [...state, {
                user : action.author,
                text : action.comment
            }];
        case 'REMOVE_COMMENT':
            //we need to return the new state without the deleted comment
            return [
                ...state.slice(0, action.index),
                //after the deleted one to the end
                ...state.slice(action.index + 1)
            ];

        default:
            return state;
    }

}