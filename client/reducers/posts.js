/**
 * @author zenteno
 * @created 1/30/17
 */

//a reducer takes in two things:

// 1. the action (information about what happened)
// 2. copy of the current state

export default function posts(state = [], action) {

    switch(action.type){
        case 'INCREMENT_LIKES':
            const i = action.index;

            //return the updated state
            console.log("Incrementing likes!");

            //... -> spread
            return [
                ...state.slice(0, i), //before the one we are updating
                {...state[i], likes : state[i].likes + 1},
                ...state.slice(i + 1)
            ];

        default:
            return state;
    }
}