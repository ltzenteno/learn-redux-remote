/**
 * @author zenteno
 * @created 1/30/17
 */

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import posts from './posts';
import comments from './comments';

//rootReducer
const rootReducer = combineReducers({posts, comments, routing : routerReducer});

export default rootReducer;