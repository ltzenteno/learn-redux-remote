/**
 * @author zenteno
 * @created 1/30/17
 */

/**
 * Increment likes
 * @param index - which post is going to have its likes incremented
 * @returns {{type: string, index: *}} Action Object
 */
export function increment(index){
    return {
        type : 'INCREMENT_LIKES',
        index
    }
}

/**
 * Add comment
 * @param postId
 * @param author
 * @param comment
 * @returns {{type: string, postId: *, author: *, comment: *}} Action Object
 */
export function addComment(postId, author, comment){
    console.log("Dispatching addComment");

    return {
        type : 'ADD_COMMENT',
        postId,
        author,
        comment
    };
}

/**
 * Remove comment
 * @param postId
 * @param index
 * @returns {{type: string, index: *, postId: *}} Action Object
 */
export function removeComment(postId, index){
    console.log("Dispatching removeComment");
    return {
        type : 'REMOVE_COMMENT',
        index,
        postId
    };
}
