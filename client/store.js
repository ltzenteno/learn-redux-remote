/**
 * @author zenteno
 * @created 1/30/17
 */

import { createStore, compose } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';

//import the root reducer
import rootReducer from './reducers/index';

import comments from './data/comments';
import posts from './data/posts';

//create an object for the default data
const defultState = {
    posts,
    comments
};

// habilitar la herramienta de Chrome para Redux
const enhancers = compose(
    //if chrome devToolsExtension existe
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

const store = createStore(rootReducer, defultState, enhancers);

export const history = syncHistoryWithStore(browserHistory, store);

// para habilitar hot reloading en reducers:

// checar si el module es hot
if(module.hot){
    //despues lo aceptamos y se ejecuta la funcion que re-require el reducer
    module.hot.accept('./reducers/', () => {
        // se usa 'require' porque no se pueden usar imports ES6 statements dentro de una funcion
        const nextRootReducer = require('./reducers/index').default;
        store.replaceReducer(nextRootReducer);
    });
}



export default store;