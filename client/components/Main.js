/**
 * Created by zenteno on 1/29/17.
 */
import React from 'react';
import { Link } from 'react-router';

export default class Main extends React.Component{

    render(){
        return (
            <div>
                <h1>
                    <Link to="/">Reduxtagram</Link>
                </h1>
                <div>
                    {React.cloneElement(this.props.children, this.props)}
                </div>
            </div>
        );
    }
}