/**
 * @author zenteno
 * @created 1/31/17
 */
import React from 'react';

export default class Comments extends React.Component{

    renderComment(comment, i){
        console.log(comment);
        return (
            <div className="comment" key={i}>
                <p>
                    <strong>{comment.user}</strong>
                    {comment.text}
                    <button onClick={this.props.removeComment.bind(null, this.props.params.postId, i)} className="remove-comment">&times;</button>
                </p>
            </div>
        );
    }

    handleSubmit(e){
        e.preventDefault();
        console.log("submitting form");

        const { postId } = this.props.params;
        const author = this.refs.author.value;
        const comment = this.refs.comment.value;

        //ejecuta funcion addComment que viene de las props
        this.props.addComment(postId, author, comment);
        this.refs.commentForm.reset();
    }

    render(){
        return (
            <div className="comments">
                {this.props.postComments.map(this.renderComment.bind(this))}
                <form ref="commentForm" className="comment-form" onSubmit={this.handleSubmit.bind(this)}>
                    <input type="text" ref="author" placeholder="author"/>
                    <input type="text" ref="comment" placeholder="comment"/>
                    <input type="submit" hidden />
                </form>
            </div>
        );
    }
}