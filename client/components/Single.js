/**
 * Created by zenteno on 1/29/17.
 */
import React from 'react';
import Photo from './Photo';
import Comments from './Comments';

export default class Single extends React.Component{

    render(){

        const { postId } = this.props.params;

        // index of the post
        //findIndex corre la array function por cada elemento en el array 'posts'
        const i = this.props.posts.findIndex((post) => post.code === postId);

        // get us the post
        const post = this.props.posts[i];
        console.log(post);

        //get comments for the post
        const postComments = this.props.comments[postId] || [];

        return (
            <div className="single-photo">
                <Photo i={i} post={post} {...this.props}/>
                <Comments postComments={postComments} params={this.props.params} addComment={this.props.addComment} removeComment={this.props.removeComment}/>
            </div>
        );
    }
}